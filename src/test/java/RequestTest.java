import io.restassured.http.ContentType;
import org.junit.Before;
import org.junit.Test;
import static io.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;

public class RequestTest {

    @Before
    public void setup(){
        baseURI = "https://jsonplaceholder.typicode.com";
    }

    @Test
    public void testRequestGet(){
        given().
            params("name", "alias odio sit").
        when().
            get("/comments").
        then().
            statusCode(200).
            body("email", contains("Lew@alysha.tv"));
    }

    @Test
    public void testRequestPost(){
         given().
            body("{{\n" +
                    "        \"name\": \"Leanne Graham\",\n" +
                    "        \"username\": \"Bret\",\n" +
                    "        \"email\": \"Sincere@april.biz\"\n" +
                    "}}").
        when().
            post("/users").
        then().
            statusCode(201).
            body("id", is(11));
    }

    @Test
    public void testRequestPut(){
        int id = 5;
        given().log().all().
            contentType(ContentType.JSON).
            body("{{\n" +
                    "    \"email\": \"teste@teste.com\",\n" +
                    "    \"address\": {\n" +
                    "        \"geo\": {\n" +
                    "            \"lat\": \"-12.3456\",\n" +
                    "            \"lng\": \"65.4321\"\n" +
                    "        }\n" +
                    "   }}\n").
        when().
            put("/users/"+ id).
        then().
            statusCode(200).
            body("id", is(id)).
            body("email", contains("teste@teste.com"));
    }
}
